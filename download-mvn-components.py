#!/usr/bin/env python3

from collections import namedtuple
from tqdm import tqdm
from urllib.parse import urlparse, urlunparse
import argparse
import binascii
import hashlib
import json
import os
import requests
import sys
import time
import traceback

parser = argparse.ArgumentParser(description='Download Maven Components')
parser.add_argument('-i', '--input', type=str, help='Json file containing components list', required=True)
parser.add_argument('--dry-run', help='Dry run, print pending urls only', action='store_true')
parser.add_argument('-o', '--output', type=str, help='Output directory', default='.')
parser.add_argument('-u', '--user', type=str, help='Basic auth user name')
parser.add_argument('-p', '--password', type=str, help='Basic auth password')
parser.add_argument('--max-retry', type=int, help='Max retry count', default=5)
args = parser.parse_args()

Asset = namedtuple('Asset', ('downloadUrl', 'path', 'id', 'repository', 'format'))


class NotFoundError(Exception):
    def __init__(self, response):
        self.request = response.request
        self.response = response


def ensure_width(s, l=20):
    if len(s) < l:
        return s + ' ' * (len(s) - l)
    else:
        return s[:l]


def file_hash(path, algo, block_size=4096):
    factory = getattr(hashlib, algo)
    hasher = factory()
    
    try:
        with open(path, 'rb') as fp:
            while True:
                chunk = fp.read(block_size)
                if chunk == b'': break
                hasher.update(chunk)
    except IOError:
        return None

    return hasher.digest()


def check_file_hash(path, algo):
    hash_path = path + '.' + algo

    if not os.path.isfile(path) or not os.path.isfile(hash_path):
        return False

    try:
        with open(hash_path, 'rb') as fp:
            hash_hexdigest = fp.read()
    except IOError:
        return False

    return hash_hexdigest == binascii.hexlify(file_hash(path, algo))


def download_with_progress(url, save_to, *, auth=None, block_size=4096, **tqdm_opts):
    dir_path, file_name = os.path.split(save_to)
    if file_name == '':
        raise ValueError('Empty file name: {!r}'.format(save_to))
    os.makedirs(dir_path, mode=0o755, exist_ok=True)

    r = requests.get(url, stream=True, auth=auth)
    if r.status_code == 404:
        raise NotFoundError(r)
    r.raise_for_status()

    total_size = int(r.headers.get('content-length', 0))
    written = 0
    with open(save_to, 'wb') as fp:
        with tqdm(desc=ensure_width(file_name), total=total_size, unit='B', unit_scale=True, **tqdm_opts) as pbar:
            for data in r.iter_content(block_size):
                fp.write(data)
                written += len(data)
                pbar.update(len(data))
        if written != total_size:
            print('{}: Early EOF, {} expected, {} written: {}'.format(url, total_size, written, url))


def download_assets(assets, *, delay=0.0, output_dir='.', **dl_opts):
    faileds = []
    not_founds = []
    
    for asset in tqdm(assets, desc=ensure_width('Downloading assets'), position=0):
        url = asset.downloadUrl
        path = asset.path
        save_to = os.path.join(output_dir, path)

        try:
            download_with_progress(url, save_to, position=1, **dl_opts)
        except NotFoundError as e:
            not_founds.append(asset)
        except requests.exceptions.RequestException as e:
            print(*traceback.format_exception(*sys.exc_info()), sep='', end='')
            faileds.append(asset)
        time.sleep(delay)
    return faileds, not_founds


def check_assets(assets, root_dir = '.'):
    faileds = []
    path_set = {asset.path for asset in assets}
    for asset in tqdm(assets, desc=ensure_width('Checking checksums')):
        path = asset.path
        file_path = os.path.join(root_dir, path)
        if path.endswith('.md5'):
            if path[:-4] in path_set:
                continue
        if path.endswith('.sha1'):
            if path[:-5] in path_set:
                continue
        md5_path = path + '.md5'
        sha1_path = path + '.sha1'
        
        md5_passed = md5_path not in path_set or check_file_hash(file_path, 'md5')
        sha1_passed = sha1_path not in path_set or check_file_hash(file_path, 'sha1')

        if not md5_passed or not sha1_passed:
            faileds.append(path)
            failed_algos = []
            if not md5_passed:
                failed_algos.append('md5')
            if not sha1_passed:
                failed_algos.append('sha1')
            if md5_path in path_set:
                faileds.append(md5_path)
            if sha1_path in path_set:
                faileds.append(sha1_path)

            if os.path.isfile(file_path):
                print('{}: {} check failed'.format(path, ' and '.join(failed_algos)))
    return faileds


def maven_metadata_assets_for(asset):
    scheme, netloc, netpath, params, query, fragment = urlparse(asset.downloadUrl)
    path = asset.path

    meta_netpaths = ['/'.join(netpath.split('/')[:i] + ['maven-metadata.xml']) for i in range(-3, 0)]
    meta_urls = [urlunparse((scheme, netloc, meta_netpath, params, query, fragment)) for meta_netpath in meta_netpaths]
    meta_paths = ['/'.join(path.split('/')[:i] + ['maven-metadata.xml']) for i in range(-3, 0)]

    for suffix in ('', '.md5', '.sha1'):
        for meta_url, meta_path in zip(meta_urls, meta_paths):
            yield Asset(meta_url + suffix, meta_path + suffix, None, asset.repository, asset.format)


def main():
    with open(args.input) as fp:
        items = json.load(fp)
    paths = [asset['path'] for item in items for asset in item['assets']]
    if args.user is not None or args.password is not None:
        auth = (args.user, args.password)
    else:
        auth = None

    assets_dicts = [asset for item in items for asset in item['assets']]
    assets = [Asset(a['downloadUrl'], a['path'], a['id'], a['repository'], a['format']) for a in assets_dicts]

    maven_metadatas = {metadata_asset for asset in assets for metadata_asset in maven_metadata_assets_for(asset)}
    assets += sorted(maven_metadatas)

    path_asset_dict = {asset.path: asset for asset in assets}
    round_num = 1
    while round_num <= args.max_retry:
        print('Round {}'.format(round_num))
        check_faileds = check_assets(assets, root_dir=args.output)
        if len(check_faileds) == 0:
            break

        assets_dl_set = {path_asset_dict[path] for path in check_faileds}
        assets_dl = sorted(assets_dl_set)
        if args.dry_run:
            for asset in assets_dl:
                print(asset.downloadUrl)
            return
        download_faileds, not_founds = download_assets(assets_dl, output_dir=args.output, auth=auth)

        for not_found in not_founds:
            assets.remove(not_found)

        print('\nRound {} has {} failed, {} not found'.format(round_num, len(download_faileds), len(not_founds)))
        round_num += 1

if __name__ == '__main__': main()
