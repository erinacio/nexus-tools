#!/usr/bin/env python3

import argparse
import json
import re
import requests
import sys
from urllib.parse import urlparse

parser = argparse.ArgumentParser(description='Maven Components')
parser.add_argument('-H', '--host', type=str, help='Host for maven repository', required=True)
parser.add_argument('-O', '--old', help='Use old rest api', action='store_true')
parser.add_argument('-o', '--output', type=str, help='Output file name')
parser.add_argument('-r', '--repo', type=str, help='Repository', required=True)
parser.add_argument('-u', '--user', type=str, help='Basic auth user name')
parser.add_argument('-p', '--password', type=str, help='Basic auth password')
args = parser.parse_args()

bad_path_re = re.compile(r'[\<\>\:\"\/\\\|\?\*]')


def get_components(url, repo, param_key, auth=None):
    params = {
            param_key: repo,
    }
    while True:
        try:
            resp = requests.get(url, params=params, auth=auth)
            resp.raise_for_status()
        except Exception as e:
            print('\nurl={!r}, params={}\n'.format(url, json.dumps(params, sort_keys=True, indent=2)), file=sys.stderr)
            raise e
        sys.stderr.write('.')
        sys.stderr.flush()
        resp_json = resp.json()
        items = resp_json['items']
        for item in items:
            yield item
        ctoken = resp_json.get('continuationToken')
        if ctoken is None:
            break
        else:
            params['continuationToken'] = ctoken
    print(file=sys.stderr)


def url_sanitize(url):
    if url.find('://') == -1:
        return 'http://{}/'.format(url.strip('/'))
    else:
        return '{}/'.format(url.strip('/'))


def dump_generator(generator, fp):
    fp.write('[')
    tail = False
    for item in generator:
        if tail:
            fp.write(',')
        else:
            tail = True
        json.dump(item, fp)
    fp.write(']')


def main():
    host = url_sanitize(args.host)
    if args.old:
        host += 'service/siesta/rest/beta/components'
        param_key = 'repositoryId'
    else:
        host += 'service/rest/beta/components'
        param_key = 'repository'

    if args.user is not None or args.password is not None:
        auth = (args.user, args.password)
    else:
        auth = None

    if args.output is None:
        output_path = bad_path_re.sub('.', urlparse(host).netloc + '.' + args.repo + '.json')
    else:
        output_path = args.output

    with open(output_path, 'w') as fp:
        dump_generator(get_components(host, args.repo, param_key, auth), fp)

if __name__ == '__main__': main()
